<?php

namespace SegayPay\Bundle\PaymentBundle\Entity;

class SagepayCustomer
{

    private $billName;
    private $products = [];
    private $customer;
    private $creditCard;
    private $billindAdress;
    private $deliveryAdress;
    private $deliveryNetAmount;

    public function setCreditCard(CreditCard $creditCard){
        $this->creditCard=$creditCard;
        return $this;
    }

    public function getCreditCard(){
        return $this->creditCard;
    }

    public function getCustomer(){
        return $this->customer;
    }

    public function setCustomer(Customer $customer){
        $this->customer = $customer;
        return $this;
    }

    public function getBillindAdress()
    {
        return $this->billindAdress;
    }

    public function setBillindAdress(BillindAdress $billindAdress){
        $this->billindAdress= $billindAdress;
        return $this;
    }

    public function getDeliveryAdress(){
        return $this->deliveryAdress;
    }

    public function setDeliveryAdress(BillindAdress $billindAdress){
        $this->deliveryAdress = $billindAdress;
        return $this;
    }

    /**
     * @param float $amount
     *
     * @return $this
     */
    public function setDeliveryNetAmount($amount)
    {
        $this->deliveryNetAmount = $amount;

        return $this;
    }

    /**
     * @return float
     */
    public function getDeliveryNetAmount()
    {
        return $this->deliveryNetAmount;
    }

    /**
     * @return mixed
     */
    public function getBillName()
    {
        return $this->billName;
    }

    /**
     * @param mixed $billName
     */
    public function setBillName($billName)
    {
        $this->billName = $billName;
    }

    public function getProducts(){
        return $this->products;
    }

    public function addProducts(Product $product){
        $this->products[] = $product;

    }
}